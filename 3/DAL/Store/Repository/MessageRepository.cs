﻿namespace DAL.Store.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DAL.Models;

    public class MessageRepository : IMessageRepository
    {
        public MessageRepository(IDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<MessageData> GetUserMessages(Guid userId)
        {
            var contacts = this.context.Contacts.Where(u => u.UserId == userId);
            List<MessageData> result = new List<MessageData>();

            foreach (var item in contacts)
                result.AddRange(item.Messages);

            return result;
        }

        public MessageData GetByContent(Guid ownerId, Guid contactId, string content)
        {
            var user = this.context.Users.FirstOrDefault(u => u.Id == ownerId);

            if (user == null)
                throw new Exception("EntityNotFoundException");

            var contact = user.ContactList.FirstOrDefault(c => c.ContactId == contactId);
            return contact.Messages.FirstOrDefault(m => m.Content.ToLower() == content.ToLower());
        }

        public MessageData Get(Guid id)
        {
            return this.context.Messages.FirstOrDefault(u => u.Id == id);
        }

        public bool Update(MessageData entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            var message = this.Get(entity.Id);

            if (message == null)
                throw new Exception("EntityNotFoundException");

            foreach (var item in message.GetType().GetProperties())
                item.SetValue(message, item.GetValue(entity));

            return this.SaveChanges();
        }

        public Guid Create(MessageData entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            var guid = Guid.NewGuid();
            entity.Id = guid;

            if (this.Get(entity.Id) != null)
                throw new Exception("AlreadyExistsException");

            this.context.Messages.Add(entity);

            if (this.SaveChanges())
                return guid;
            else
                return Guid.Empty;
        }

        public bool Remove(MessageData entity)
        {
            this.context.Messages.Remove(entity);
            return this.SaveChanges();
        }

        private bool SaveChanges()
        {
            return this.context.SaveChanges() > 0;
        }

        public void Dispose()
        {
            this.context.Dispose();
        }

        private readonly IDbContext context;
    }
}
