﻿namespace DAL.Store.Repository
{
    using Models;
    using System;
    using System.Collections.Generic;

    public interface IContactRepository : IRepository<ContactData>
    {
        IEnumerable<ContactData> GetUserContacts(Guid userId);
        ContactData GetByUserName(Guid ownerId, string contactUserName);
    }
}
