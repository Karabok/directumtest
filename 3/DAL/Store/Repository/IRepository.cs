﻿namespace DAL.Store.Repository
{
    using Models;
    using System;

    public interface IRepository<T> : IDisposable where T: IEntity
    {
        T Get(Guid id);
        bool Update(T entity);
        Guid Create(T entity);
        bool Remove(T entity);
    }
}
