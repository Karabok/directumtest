﻿namespace DAL.Store.Repository
{
    using Models;
    using System;
    using System.Collections.Generic;

    public interface IUserRepository : IRepository<UserData>
    {
        UserData GetByName(string name);
        /// <summary>
        /// Регистронезависимый поиск списка юзеров по имени, в том числе по части имени.
        /// </summary>
        IEnumerable<UserData> SearchByName(string name);
        bool UpdateUserState(Guid userId, UserStates newState);
    }
}
