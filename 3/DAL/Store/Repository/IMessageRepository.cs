﻿namespace DAL.Store.Repository
{
    using Models;
    using System;
    using System.Collections.Generic;

    public interface IMessageRepository : IRepository<MessageData>
    {
        IEnumerable<MessageData> GetUserMessages(Guid userId);
        MessageData GetByContent(Guid ownerId, Guid contactId, string content);
    }
}
