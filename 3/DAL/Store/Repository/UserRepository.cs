﻿namespace DAL.Store.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DAL.Models;

    public class UserRepository : IUserRepository
    {
        public UserRepository(IDbContext context)
        {
            this.context = context;
        }

        public UserData GetByName(string name)
        {
            return this.context.Users.FirstOrDefault(u => u.Name == name);
        }

        /// <summary>
        /// Регистронезависимый поиск списка юзеров по имени, в том числе по части имени.
        /// </summary>
        public IEnumerable<UserData> SearchByName(string name)
        {
            return this.context.Users.Where(u => u.Name.ToLower().Contains(name.ToLower())).ToList();
        }

        public bool UpdateUserState(Guid userId, UserStates newState)
        {
            var user = this.Get(userId);

            if (user == null)
                throw new Exception("EntityNotFoundException");

            user.State = newState;
            return this.SaveChanges();
        }

        public UserData Get(Guid id)
        {
            return this.context.Users.FirstOrDefault(u => u.Id == id);
        }

        public bool Update(UserData entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            var user = this.Get(entity.Id);

            if (user == null)
                throw new Exception("EntityNotFoundException");

            foreach (var item in user.GetType().GetProperties())
                item.SetValue(user, item.GetValue(entity));

            return this.SaveChanges();
        }

        public Guid Create(UserData entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            var guid = Guid.NewGuid();
            entity.Id = guid;

            if (this.Get(entity.Id) != null)
                throw new Exception("AlreadyExistsException");

            this.context.Users.Add(entity);

            if (this.SaveChanges())
                return guid;
            else
                return Guid.Empty;
        }

        public bool Remove(UserData entity)
        {
            this.context.Users.Remove(entity);
            return this.SaveChanges();
        }

        private bool SaveChanges()
        {
            return this.context.SaveChanges() > 0;
        }

        public void Dispose()
        {
            this.context.Dispose();
        }

        private readonly IDbContext context;
    }
}
