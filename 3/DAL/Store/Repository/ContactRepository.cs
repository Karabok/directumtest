﻿namespace DAL.Store.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using DAL.Models;

    public class ContactRepository : IContactRepository
    {
        public ContactRepository(IDbContext context)
        {
            this.context = context;
        }

        public IEnumerable<ContactData> GetUserContacts(Guid userId)
        {
            var user = this.context.Users.FirstOrDefault(u => u.Id == userId);

            if (user == null)
                throw new Exception("EntityNotFoundException");

            return user.ContactList;
        }

        public ContactData GetByUserName(Guid ownerId, string contactUserName)
        {
            var user = this.context.Users.FirstOrDefault(u => u.Id == ownerId);

            if (user == null)
                throw new Exception("EntityNotFoundException");

            return user.ContactList.FirstOrDefault(c => c.Contact?.Name == contactUserName);
        }

        public ContactData Get(Guid id)
        {
            return this.context.Contacts.FirstOrDefault(u => u.Id == id);
        }

        public bool Update(ContactData entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            var contact = this.Get(entity.Id);

            if (contact == null)
                throw new Exception("EntityNotFoundException");

            foreach (var item in contact.GetType().GetProperties())
                item.SetValue(contact, item.GetValue(entity));

            return this.SaveChanges();
        }

        public Guid Create(ContactData entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            var guid = Guid.NewGuid();
            entity.Id = guid;

            if (this.Get(entity.Id) != null)
                throw new Exception("AlreadyExistsException");

            this.context.Contacts.Add(entity);

            if (this.SaveChanges())
                return guid;
            else
                return Guid.Empty;
        }

        public bool Remove(ContactData entity)
        {
            this.context.Contacts.Remove(entity);
            return this.SaveChanges();
        }

        private bool SaveChanges()
        {
            return this.context.SaveChanges() > 0;
        }

        public void Dispose()
        {
            this.context.Dispose();
        }

        private readonly IDbContext context;
    }
}
