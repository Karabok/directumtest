﻿namespace DAL.Store
{
    using DAL.Models;
    using Microsoft.EntityFrameworkCore;

    public class DbContext : Microsoft.EntityFrameworkCore.DbContext, IDbContext
    {
        public DbContext(DbContextOptions<DbContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<UserData> Users { get; set; }
        public DbSet<ContactData> Contacts { get; set; }
        public DbSet<MessageData> Messages { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseLazyLoadingProxies();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserData>().HasMany(u => u.ContactList).WithOne(c => c.User).HasForeignKey(c => c.UserId);
            modelBuilder.Entity<UserData>().HasMany(u => u.Contacts).WithOne(c => c.Contact).HasForeignKey(c => c.ContactId);
            modelBuilder.Entity<ContactData>().HasMany(c => c.Messages).WithOne(m => m.Contact).HasForeignKey(c => c.ContactId);
        }
    }
}
