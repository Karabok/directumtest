﻿namespace DAL.Store
{
    using DAL.Models;
    using Microsoft.EntityFrameworkCore;
    using System;

    public interface IDbContext : IDisposable
    {
        DbSet<UserData> Users { get; set; }
        DbSet<ContactData> Contacts { get; set; }
        DbSet<MessageData> Messages { get; set; }
        int SaveChanges();
    }
}
