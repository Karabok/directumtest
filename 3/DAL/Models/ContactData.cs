﻿namespace DAL.Models
{
    using System;
    using System.Collections.Generic;

    public class ContactData : IEntity
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; } // владелец контакта
        public virtual UserData User { get; set; }
        public Guid ContactId { get; set; }
        public virtual UserData Contact { get; set; } // контакт
        public DateTime LastUpdateTime { get; set; }
        public IEnumerable<MessageData> Messages { get; set; }

        public override bool Equals(object obj)
        {
            if (obj is ContactData other)
            {
                return this.Id == other.Id
                    && this.UserId == other.UserId
                    && this.LastUpdateTime == other.LastUpdateTime
                    && this.ContactId == other.ContactId;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
