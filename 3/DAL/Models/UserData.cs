﻿namespace DAL.Models
{
    using System;
    using System.Collections.Generic;

    public enum UserStates
    {
        Online,
        Offline
    }

    public class UserData : IEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public UserStates State { get; set; }
        /// <summary>
        /// Список контактов, где юзер является владельцем.
        /// </summary>
        public IEnumerable<ContactData> ContactList { get; set; }
        /// <summary>
        /// Cписок контактов, где юзер является адресатом.
        /// </summary>
        public IEnumerable<ContactData> Contacts { get; set; } // todo: придумать нормальное название

        /// <summary>
        /// Упрощенный метод сравнения объектов (без сравнения коллекций).
        /// </summary>
        public override bool Equals(object obj)
        {
            if(obj is UserData other)
            {
                return this.Id == other.Id 
                    && this.Name == other.Name
                    && this.Password == other.Password
                    && this.State == other.State;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
