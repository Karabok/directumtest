﻿namespace DAL.Models
{
    using System;

    public interface IEntity
    {
        Guid Id { get; set; }
    }
}
