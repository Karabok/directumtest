﻿namespace DAL.Models
{
    using System;

    public class MessageData : IEntity
    {
        public Guid Id { get; set; }
        public Guid ContactId { get; set; }
        public ContactData Contact { get; set; }
        public DateTime SendTime { get; set; }
        public DateTime DeliveryTime { get; set; }
        public string Content { get; set; }

        public override bool Equals(object obj)
        {
            if(obj is MessageData other)
            {
                return this.Id == other.Id 
                    && this.SendTime == other.SendTime
                    && this.DeliveryTime == other.DeliveryTime
                    && this.Content == other.Content
                    && this.ContactId == other.ContactId;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}