namespace Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using DAL.Store;
    using DAL.Models;
    using System.Collections.Generic;
    using System;
    using System.Linq;
    using Moq;
    using Microsoft.EntityFrameworkCore;
    using DAL.Store.Repository;
    using Test.Moqs;

    [TestClass]
    public class UserRepositoryTests
    {
        [TestInitialize]
        public void Init()
        {
            var users = new List<UserData>
            {
                new UserData
                {
                    Id = user1Id,
                    Name = "Max",
                    State = UserStates.Online,
                    Password = "123",
                    ContactList = new List<ContactData>
                    {
                        new ContactData
                        {
                            Id = Guid.NewGuid(),
                            LastUpdateTime = DateTime.Now,
                            Contact = new UserData
                            {
                                Id = Guid.NewGuid(),
                                Name = "Ivan",
                                State = UserStates.Offline,
                                Password = "123"
                            }
                        }
                    }
                },
                new UserData
                {
                    Id = user2Id,
                    Name = "Oleg",
                    Password = "123",
                    State = UserStates.Offline
                },
                new UserData
                {
                    Id = user3Id,
                    Name = "MAX",
                    Password = "123",
                    State = UserStates.Offline
                }
            };
            this.context = new Mock<IDbContext>();
            this.context.Setup(c => c.Users).Returns(MoqFabric.GetQueryableMockDbSet(users));
            this.context.Setup(c => c.SaveChanges()).Returns(1);
            this.repository = new UserRepository(context.Object);
        }

        [TestMethod]
        public void GetByNameTest()
        {
            var expectedUser = new UserData
            {
                Id = user1Id,
                Name = "Max",
                Password = "123",
                State = UserStates.Online
            };

            var foundUser = this.repository.GetByName("Max");

            if (foundUser == null)
                Assert.Fail("foundUser is null");

            Assert.AreEqual(expectedUser, foundUser);
        }

        [TestMethod]
        public void SearchByNameTest()
        {
            var expectedUsers = new List<UserData>
            {
                new UserData
                {
                    Id = user1Id,
                    Name = "Max",
                    Password = "123",
                    State = UserStates.Online
                },
                new UserData
                {
                    Id = user3Id,
                    Name = "MAX",
                    Password = "123",
                    State = UserStates.Offline
                }
            };
            var foundUsers = this.repository.SearchByName("max");
            Assert.IsTrue(expectedUsers.All(u => foundUsers.Contains(u)));
        }

        [TestMethod]
        public void UpdateUserStateTest()
        {
            this.repository.UpdateUserState(user1Id, UserStates.Offline);
            Assert.IsTrue(this.context.Object.Users.FirstOrDefault(u => u.Id == user1Id).State == UserStates.Offline);
        }

        [TestMethod]
        public void GetTest()
        {
            var expectedUser = new UserData
            {
                Id = user1Id,
                Name = "Max",
                State = UserStates.Online,
                Password = "123",
            };
            var foundUser = this.repository.Get(user1Id);
            Assert.AreEqual(expectedUser, foundUser);
        }

        [TestMethod]
        public void UpdateTest()
        {
            var newUser = new UserData
            {
                Id = user2Id,
                Name = "Oleg123",
                Password = "999",
                State = UserStates.Offline
            };
            this.repository.Update(newUser);
            Assert.AreEqual(this.context.Object.Users.FirstOrDefault(u => u.Id == user2Id), newUser);
        }

        [TestMethod]
        public void CreateTest()
        {
            var newUser = new UserData
            {
                Name = "Test",
                Password = "12345",
                State = UserStates.Offline
            };
            var foundGuid = this.repository.Create(newUser);
            Assert.IsNotNull(this.context.Object.Users.FirstOrDefault(u => u.Id == foundGuid));
        }

        [TestMethod]
        public void RemoveTest()
        {
            var user = new UserData
            {
                Id = user3Id,
                Name = "MAX",
                Password = "123",
                State = UserStates.Offline
            };
            this.repository.Remove(user);
            Assert.IsTrue(this.context.Object.Users.Count() == 2);
        }

        private Mock<IDbContext> context;
        private IUserRepository repository;
        private Guid user1Id = Guid.NewGuid();
        private Guid user2Id = Guid.NewGuid();
        private Guid user3Id = Guid.NewGuid();
    }
}
