﻿namespace Test
{
    using DAL.Models;
    using DAL.Store;
    using DAL.Store.Repository;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Moqs;

    [TestClass]
    public class ContactRepositoryTest
    {
        [TestInitialize]
        public void Init()
        {
            this.contact1 = new ContactData
            {
                Id = Guid.NewGuid(),
                ContactId = user3Id,
                UserId = user1Id,
                User = user1,
                Messages = new List<MessageData>(),
                LastUpdateTime = DateTime.MinValue,
            };
            this.contact2 = new ContactData
            {
                Id = Guid.NewGuid(),
                ContactId = user3Id,
                UserId = user2Id,
                Messages = new List<MessageData>(),
                LastUpdateTime = DateTime.MinValue,
            };
            this.contact3 = new ContactData
            {
                Id = Guid.NewGuid(),
                ContactId = user1Id,
                UserId = user3Id,
                Messages = new List<MessageData>(),
                LastUpdateTime = DateTime.MinValue,
            };
            this.user1 = new UserData
            {
                Id = user1Id,
                Name = "Max",
                State = UserStates.Online,
                Password = "123",
                ContactList = new List<ContactData>
                {
                    this.contact1
                }
            };
            this.user2 = new UserData
            {
                Id = user2Id,
                Name = "Oleg",
                State = UserStates.Online,
                Password = "123",
                ContactList = new List<ContactData>
                {
                    this.contact2
                }
            };
            this.user3 = new UserData
            {
                Id = user3Id,
                Name = "Ilya",
                State = UserStates.Online,
                Password = "123",
                ContactList = new List<ContactData>
                {
                    this.contact3
                },
                Contacts = new List<ContactData>
                {
                    this.contact1
                }
            };

            this.contact1.Contact = this.user3;
            this.contact2.Contact = this.user1;
            this.contact3.Contact = this.user1;

            var contacts = new List<ContactData>
            {
                this.contact1,
                this.contact2,
                this.contact3
            };
            var users = new List<UserData>
            {
                this.user1,
                this.user2,
                this.user3
            };
            this.context = new Mock<IDbContext>();
            this.context.Setup(c => c.Users).Returns(MoqFabric.GetQueryableMockDbSet(users));
            this.context.Setup(c => c.Contacts).Returns(MoqFabric.GetQueryableMockDbSet(contacts));
            this.context.Setup(c => c.SaveChanges()).Returns(1);
            this.repository = new ContactRepository(context.Object);
        }

        [TestMethod]
        public void GetUserContactsTest()
        {
            var expectedContacts = new List<ContactData>
            {
                this.contact1
            };

            var foundContacts = this.repository.GetUserContacts(user1Id);

            if (foundContacts == null || !foundContacts.Any())
                Assert.Fail("Contact not found");

            Assert.IsTrue(expectedContacts.All(m => foundContacts.Contains(m)));
        }

        [TestMethod]
        public void GetByUserNameTest()
        {
            var expectedContact = this.contact1;
            var foundContact = this.repository.GetByUserName(user1Id, user3.Name);
            Assert.AreEqual(expectedContact, foundContact);
        }

        [TestMethod]
        public void GetTest()
        {
            var expectedContact = this.contact1;
            var foundContact = this.repository.Get(contact1.Id);
            Assert.AreEqual(expectedContact, foundContact);
        }

        [TestMethod]
        public void UpdateTest()
        {
            var newContact = new ContactData
            {
                Id = this.contact1.Id,
                ContactId = this.contact1.ContactId,
                UserId = this.contact1.UserId,
                LastUpdateTime = this.contact1.LastUpdateTime
            };
            this.repository.Update(newContact);
            Assert.AreEqual(this.context.Object.Contacts.FirstOrDefault(m => m.Id == this.contact1.Id), newContact);
        }

        [TestMethod]
        public void CreateTest()
        {
            var newContact = new ContactData
            {
                ContactId = user3.Id,
                UserId = user2.Id,
                LastUpdateTime = DateTime.Now
            };
            var newId = this.repository.Create(newContact);
            var foundContact = this.context.Object.Contacts.FirstOrDefault(m => m.Id == newId);

            if (foundContact == null)
                Assert.Fail("Contact not found");

            Assert.AreEqual(foundContact, newContact);
        }

        [TestMethod]
        public void RemoveTest()
        {
            this.repository.Remove(contact1);
            Assert.IsTrue(this.context.Object.Contacts.Count() == 2);
        }

        private Mock<IDbContext> context;
        private IContactRepository repository;
        private ContactData contact1;
        private ContactData contact2;
        private ContactData contact3;
        private Guid user1Id = Guid.NewGuid();
        private Guid user2Id = Guid.NewGuid();
        private Guid user3Id = Guid.NewGuid();
        private UserData user1;
        private UserData user2;
        private UserData user3;
    }
}