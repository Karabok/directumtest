﻿namespace Test
{
    using DAL.Models;
    using DAL.Store;
    using DAL.Store.Repository;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Moqs;

    [TestClass]
    public class MessageRepositoryTests
    {
        [TestInitialize]
        public void Init()
        {
            this.message1 = new MessageData
            {
                Id = mes1Id,
                Content = "Test message",
                DeliveryTime = DateTime.MaxValue,
                SendTime = DateTime.MinValue,
                ContactId = cont1Id,
                Contact = contact1
            };
            this.message2 = new MessageData
            {
                Id = mes2Id,
                Content = "Test message2",
                DeliveryTime = DateTime.MaxValue,
                SendTime = DateTime.MinValue,
                ContactId = cont1Id,
                Contact = contact1
            };
            this.message3 = new MessageData
            {
                Id = mes3Id,
                Content = "Test message3",
                DeliveryTime = DateTime.MaxValue,
                SendTime = DateTime.MinValue,
                ContactId = cont3Id,
                Contact = contact2
            };
            this.contact1 = new ContactData
            {
                Id = cont1Id,
                UserId = user1Id,
                LastUpdateTime = DateTime.MinValue,
                ContactId = user3Id,
                Messages = new List<MessageData>
                {
                    message1,
                    message2
                }
            };
            this.contact2 = new ContactData
            {
                Id = cont2Id,
                UserId = user2Id,
                LastUpdateTime = DateTime.MinValue,
                ContactId = user3Id,
                Messages = new List<MessageData>
                {
                    message3
                }
            };
            var contacts = new List<ContactData>
            {
                contact1,
                contact2
            };
            var messages = new List<MessageData>
            {
                message1,
                message2,
                message3
            };
            var users = new List<UserData>
            {
                new UserData
                {
                    Id = user1Id,
                    Name = "Max",
                    State = UserStates.Online,
                    Password = "123",
                    ContactList = new List<ContactData>
                    {
                        contact1
                    }
                },
                new UserData
                {
                    Id = user2Id,
                    Name = "Oleg",
                    State = UserStates.Offline,
                    Password = "123",
                    ContactList = new List<ContactData>
                    {
                        contact2
                    }
                }
            };
            this.context = new Mock<IDbContext>();
            this.context.Setup(c => c.Contacts).Returns(MoqFabric.GetQueryableMockDbSet(contacts));        
            this.context.Setup(c => c.Messages).Returns(MoqFabric.GetQueryableMockDbSet(messages));       
            this.context.Setup(c => c.Users).Returns(MoqFabric.GetQueryableMockDbSet(users));
            this.context.Setup(c => c.SaveChanges()).Returns(1);
            this.repository = new MessageRepository(context.Object);
            
        }

        [TestMethod]
        public void GetUserMessagesTest()
        {
            var expectedmessages = new List<MessageData>
            {
                new MessageData
                {
                    Id = mes1Id,
                    Content = "Test message",
                    DeliveryTime = DateTime.MaxValue,
                    SendTime = DateTime.MinValue,
                    ContactId = cont1Id,
                    Contact = contact1
                },
                new MessageData
                {
                    Id = mes2Id,
                    Content = "Test message2",
                    DeliveryTime = DateTime.MaxValue,
                    SendTime = DateTime.MinValue,
                    ContactId = cont1Id,
                    Contact = contact1
                }
            };
            var foundMessages = this.repository.GetUserMessages(user1Id);

            if (foundMessages == null || !foundMessages.Any())
                Assert.Fail("Messages not found");

            Assert.IsTrue(expectedmessages.All(m => foundMessages.Contains(m)));
        }

        [TestMethod]
        public void GetByContentTest()
        {
            var expectedMessage = message3;
            var foundMessage = this.repository.GetByContent(user2Id, user3Id, "TEST message3");

            if (foundMessage == null)
                Assert.Fail("Message not found");

            Assert.AreEqual(expectedMessage, foundMessage);
        }

        [TestMethod]
        public void GetTest()
        {
            var expectedMessage = this.message1;
            var foundMessage = this.repository.Get(expectedMessage.Id);

            if (foundMessage == null)
                Assert.Fail("Message not found");

            Assert.AreEqual(expectedMessage, foundMessage);
        }

        [TestMethod]
        public void UpdateTest()
        {
            var newMessage = new MessageData
            {
                Id = this.message1.Id,
                ContactId = this.message1.ContactId,
                Content = this.message1.Content + " 123456789",
                DeliveryTime = this.message1.DeliveryTime,
                SendTime = this.message1.SendTime
            };
            this.repository.Update(newMessage);
            Assert.AreEqual(this.context.Object.Messages.FirstOrDefault(m => m.Id == this.message1.Id), newMessage);
        }

        [TestMethod]
        public void CreateTest()
        {
            var newMessage = new MessageData
            {
                ContactId = cont2Id,
                DeliveryTime = DateTime.Now,
                SendTime = DateTime.Now,
                Content = "Created in test"
            };
            var newId = this.repository.Create(newMessage);
            var foundMessage = this.context.Object.Messages.FirstOrDefault(m => m.Id == newId);

            if (foundMessage == null)
                Assert.Fail("Message not found");

            Assert.AreEqual(foundMessage, newMessage);
        }

        [TestMethod]
        public void RemoveTest()
        {
            this.repository.Remove(message1);
            Assert.IsTrue(this.context.Object.Messages.Count() == 2);
        }

        private Mock<IDbContext> context;
        private IMessageRepository repository;
        private Guid mes1Id = Guid.NewGuid();
        private Guid mes2Id = Guid.NewGuid();
        private Guid mes3Id = Guid.NewGuid();
        private Guid cont1Id = Guid.NewGuid();
        private Guid cont2Id = Guid.NewGuid();
        private Guid cont3Id = Guid.NewGuid();
        private Guid user1Id = Guid.NewGuid();
        private Guid user2Id = Guid.NewGuid();
        private Guid user3Id = Guid.NewGuid();
        private ContactData contact1;
        private ContactData contact2;
        private MessageData message1;
        private MessageData message2;
        private MessageData message3;
    }
}
