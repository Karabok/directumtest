namespace _4.Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using CodeAnalysis.Models;
    using CodeAnalysis.Models.Types;

    [TestClass]
    public class MatrixTest
    {
        [TestInitialize]
        public void Init()
        {
            this.InitIntMatrixes();
            this.InitVectorMatrixes();
            this.InitComplexMatrixes();
        }

        [TestMethod]
        public void IntMatrixMultiplyTest()
        {
            var expectedMatrix = new Matrix<Double>(3, 3);
            expectedMatrix[0, 0] = new Double(30);
            expectedMatrix[0, 1] = new Double(24);
            expectedMatrix[0, 2] = new Double(18);
            expectedMatrix[1, 0] = new Double(84);
            expectedMatrix[1, 1] = new Double(69);
            expectedMatrix[1, 2] = new Double(54);
            expectedMatrix[2, 0] = new Double(138);
            expectedMatrix[2, 1] = new Double(114);
            expectedMatrix[2, 2] = new Double(90);

            Matrix<Double> resultMatrix = this.firstIntMatrix.Multiply(this.secondIntMatrix);

            Assert.AreEqual(expectedMatrix, resultMatrix);
        }

        [TestMethod]
        public void VectorMatrixMultiplyTest()
        {
            var expectedMatrix = new Matrix<Double>(3, 3);
            expectedMatrix[0, 0] = new Double(60);
            expectedMatrix[0, 1] = new Double(48);
            expectedMatrix[0, 2] = new Double(36);
            expectedMatrix[1, 0] = new Double(168);
            expectedMatrix[1, 1] = new Double(138);
            expectedMatrix[1, 2] = new Double(108);
            expectedMatrix[2, 0] = new Double(276);
            expectedMatrix[2, 1] = new Double(228);
            expectedMatrix[2, 2] = new Double(180);

            Matrix<Double> resultMatrix = this.firstVectorMatrix.Multiply(this.secondVectorMatrix);

            Assert.AreEqual(expectedMatrix, resultMatrix);
        }

        [TestMethod]
        public void ComplexMatrixMultiplyTest()
        {
            var expectedMatrix = new Matrix<Double>(3, 3);
            expectedMatrix[0, 0] = new Double(60);
            expectedMatrix[0, 1] = new Double(48);
            expectedMatrix[0, 2] = new Double(36);
            expectedMatrix[1, 0] = new Double(168);
            expectedMatrix[1, 1] = new Double(138);
            expectedMatrix[1, 2] = new Double(108);
            expectedMatrix[2, 0] = new Double(276);
            expectedMatrix[2, 1] = new Double(228);
            expectedMatrix[2, 2] = new Double(180);

            Matrix<Double> resultMatrix = this.firstComplexMatrix.Multiply(this.secondComplexMatrix);

            Assert.AreEqual(expectedMatrix, resultMatrix);
        }

        private void InitIntMatrixes()
        {
            this.firstIntMatrix = new Matrix<Integer>(3, 3);
            this.secondIntMatrix = new Matrix<Integer>(3, 3);

            this.firstIntMatrix[0, 0] = new Integer(1);
            this.firstIntMatrix[0, 1] = new Integer(2);
            this.firstIntMatrix[0, 2] = new Integer(3);
            this.firstIntMatrix[1, 0] = new Integer(4);
            this.firstIntMatrix[1, 1] = new Integer(5);
            this.firstIntMatrix[1, 2] = new Integer(6);
            this.firstIntMatrix[2, 0] = new Integer(7);
            this.firstIntMatrix[2, 1] = new Integer(8);
            this.firstIntMatrix[2, 2] = new Integer(9);

            this.secondIntMatrix[0, 0] = new Integer(9);
            this.secondIntMatrix[0, 1] = new Integer(8);
            this.secondIntMatrix[0, 2] = new Integer(7);
            this.secondIntMatrix[1, 0] = new Integer(6);
            this.secondIntMatrix[1, 1] = new Integer(5);
            this.secondIntMatrix[1, 2] = new Integer(4);
            this.secondIntMatrix[2, 0] = new Integer(3);
            this.secondIntMatrix[2, 1] = new Integer(2);
            this.secondIntMatrix[2, 2] = new Integer(1);
        }

        private void InitVectorMatrixes()
        {
            this.firstVectorMatrix = new Matrix<Vector>(3, 3);
            this.secondVectorMatrix = new Matrix<Vector>(3, 3);

            this.firstVectorMatrix[0, 0] = new Vector(1, 1);
            this.firstVectorMatrix[0, 1] = new Vector(2, 2);
            this.firstVectorMatrix[0, 2] = new Vector(3, 3);
            this.firstVectorMatrix[1, 0] = new Vector(4, 4);
            this.firstVectorMatrix[1, 1] = new Vector(5, 5);
            this.firstVectorMatrix[1, 2] = new Vector(6, 6);
            this.firstVectorMatrix[2, 0] = new Vector(7, 7);
            this.firstVectorMatrix[2, 1] = new Vector(8, 8);
            this.firstVectorMatrix[2, 2] = new Vector(9, 9);

            this.secondVectorMatrix[0, 0] = new Vector(9, 9);
            this.secondVectorMatrix[0, 1] = new Vector(8, 8);
            this.secondVectorMatrix[0, 2] = new Vector(7, 7);
            this.secondVectorMatrix[1, 0] = new Vector(6, 6);
            this.secondVectorMatrix[1, 1] = new Vector(5, 5);
            this.secondVectorMatrix[1, 2] = new Vector(4, 4);
            this.secondVectorMatrix[2, 0] = new Vector(3, 3);
            this.secondVectorMatrix[2, 1] = new Vector(2, 2);
            this.secondVectorMatrix[2, 2] = new Vector(1, 1);
        }

        private void InitComplexMatrixes()
        {
            this.firstComplexMatrix = new Matrix<Complex>(3, 3);
            this.secondComplexMatrix = new Matrix<Complex>(3, 3);

            this.firstComplexMatrix[0, 0] = new Complex(1, 1);
            this.firstComplexMatrix[0, 1] = new Complex(2, 2);
            this.firstComplexMatrix[0, 2] = new Complex(3, 3);
            this.firstComplexMatrix[1, 0] = new Complex(4, 4);
            this.firstComplexMatrix[1, 1] = new Complex(5, 5);
            this.firstComplexMatrix[1, 2] = new Complex(6, 6);
            this.firstComplexMatrix[2, 0] = new Complex(7, 7);
            this.firstComplexMatrix[2, 1] = new Complex(8, 8);
            this.firstComplexMatrix[2, 2] = new Complex(9, 9);

            this.secondComplexMatrix[0, 0] = new Complex(9, 9);
            this.secondComplexMatrix[0, 1] = new Complex(8, 8);
            this.secondComplexMatrix[0, 2] = new Complex(7, 7);
            this.secondComplexMatrix[1, 0] = new Complex(6, 6);
            this.secondComplexMatrix[1, 1] = new Complex(5, 5);
            this.secondComplexMatrix[1, 2] = new Complex(4, 4);
            this.secondComplexMatrix[2, 0] = new Complex(3, 3);
            this.secondComplexMatrix[2, 1] = new Complex(2, 2);
            this.secondComplexMatrix[2, 2] = new Complex(1, 1);
        }

        private Matrix<Integer> firstIntMatrix;
        private Matrix<Integer> secondIntMatrix;
        private Matrix<Vector> firstVectorMatrix;
        private Matrix<Vector> secondVectorMatrix;
        private Matrix<Complex> firstComplexMatrix;
        private Matrix<Complex> secondComplexMatrix;
    }
}
