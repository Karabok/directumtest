﻿namespace CodeAnalysis.Models
{
    using System;
    using Types;

    public class Matrix<T> where T: IMatrixType
    {
        public Matrix(int rowCount, int colCount)
        {
            this.RowCount = rowCount;
            this.ColCount = colCount;
            this.data = new T[this.RowCount, this.ColCount];
        }

        public Matrix<Types.Double> Multiply(Matrix<T> matrix)
        {
            if (matrix == null)
                throw new ArgumentNullException("matrix");

            if (this.ColCount != matrix.RowCount)
                throw new ArgumentOutOfRangeException(nameof(matrix));

            Matrix<Types.Double> result = new Matrix<Types.Double>(this.RowCount, matrix.ColCount);

            for (int i = 0; i < this.RowCount; i++)
            {
                for (int j = 0; j < matrix.ColCount; j++)
                {
                    for (int k = 0; k < matrix.RowCount; k++)
                    {
                        result[i, j] += data[i, k].Multiply(matrix[k, j]);
                    }
                }
            }

            return result;
        }

        public override bool Equals(object obj)
        {
            if(obj is Matrix<T> other)
            {
                if (this.ColCount != other.ColCount || this.RowCount != other.RowCount)
                    return false;

                for (int i = 0; i < this.RowCount; i++)
                {
                    for (int j = 0; j < this.ColCount; j++)
                    {
                        if (!this[i, j].Equals(other[i, j]))
                            return false;
                    }
                }

                return true;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public T this[int i, int j]
        {
            get
            {
                return this.data[i, j];
            }
            set
            {
                this.data[i, j] = value;
            }
        }

        public int RowCount { get; private set; }
        public int ColCount { get; private set; }
        private readonly T[,] data;
    }
}
