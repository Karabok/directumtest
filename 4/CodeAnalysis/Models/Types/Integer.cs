﻿namespace CodeAnalysis.Models.Types
{
    public struct Integer : IMatrixType
    {
        public Integer(int value)
        {
            this.Value = value;
        }

        public Double Multiply(IMatrixType other)
        {
            Integer otherInt = (Integer)other;
            return new Double(this.Value * otherInt.Value);
        }

        public override bool Equals(object obj)
        {
            if(obj is Integer other)
            {
                return this.Value == other.Value;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return this.Value.ToString();
        }

        public int Value { get; private set; }
    }
}
