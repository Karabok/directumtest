﻿namespace CodeAnalysis.Models.Types
{
    public struct Complex : IMatrixType
    {
        public Complex(double re, double im)
        {
            this.Re = re;
            this.Im = im;
        }

        public Double Multiply(IMatrixType other)
        {
            Complex otherComplex = (Complex)other;
            return new Double((this.Re * otherComplex.Re - this.Im * otherComplex.Im) 
                            + (this.Re * otherComplex.Im + this.Im * otherComplex.Re));
        }

        public override bool Equals(object obj)
        {
            if (obj is Complex other)
            {
                return this.Re == other.Re
                    && this.Im == other.Re;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return $"{this.Re} + {this.Im}i";
        }

        public double Re { get; private set; }
        public double Im { get; private set; }
    }
}
