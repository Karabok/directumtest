﻿namespace CodeAnalysis.Models.Types
{
    public struct Vector : IMatrixType
    {
        public Vector(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public Double Multiply(IMatrixType other)
        {
            Vector otherVector = (Vector)other;
            return new Double(this.X * otherVector.X + this.Y * otherVector.Y);
        }

        public override bool Equals(object obj)
        {
            if (obj is Vector other)
            {
                return this.X == other.X
                    && this.Y == other.Y;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return $"[{this.X};{this.Y}]";
        }

        public int X { get; private set; }
        public int Y { get; private set; }

    }
}
