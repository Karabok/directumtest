﻿namespace CodeAnalysis.Models.Types
{
    /// <summary>
    /// Матричный тип.
    /// </summary>
    public interface IMatrixType
    {
        /// <summary>
        /// Выполняет умножение экземпляра матричного типа на другой.
        /// </summary>
        /// <returns>Результаты умножения всех матричных типов можо привести к double без потери точности.</returns>
        Double Multiply(IMatrixType other);
    }
}
