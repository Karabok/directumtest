﻿namespace CodeAnalysis.Models.Types
{
    public struct Double : IMatrixType
    {
        public Double(double value)
        {
            this.Value = value;
        }

        public Double Multiply(IMatrixType other)
        {
            Double otherDouble = (Double)other;
            return new Double(this.Value * otherDouble.Value);
        }

        public static Double operator +(Double x, Double y)
        {
            return new Double(x.Value + y.Value);
        }

        public override bool Equals(object obj)
        {
            if (obj is Double other)
            {
                return this.Value == other.Value;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return this.Value.ToString();
        }

        public double Value { get; private set; }
    }
}
